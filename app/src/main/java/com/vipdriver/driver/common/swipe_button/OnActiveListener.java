package com.vipdriver.driver.common.swipe_button;

public interface OnActiveListener {
    void onActive();
}

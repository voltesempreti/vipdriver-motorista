package com.vipdriver.driver.common.swipe_button;

public interface OnStateChangeListener {
    void onStateChange(boolean active);
}

package com.vipdriver.driver.ui.activity.password;

import com.vipdriver.driver.base.MvpView;
import com.vipdriver.driver.data.network.model.ForgotResponse;
import com.vipdriver.driver.data.network.model.User;

public interface PasswordIView extends MvpView {

    void onSuccess(ForgotResponse forgotResponse);

    void onSuccess(User object);

    void onError(Throwable e);
}

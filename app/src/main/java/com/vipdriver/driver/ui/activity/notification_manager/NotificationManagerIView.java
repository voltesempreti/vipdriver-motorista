package com.vipdriver.driver.ui.activity.notification_manager;

import com.vipdriver.driver.base.MvpView;
import com.vipdriver.driver.data.network.model.NotificationManager;

import java.util.List;

public interface NotificationManagerIView extends MvpView {

    void onSuccess(List<NotificationManager> managers);

    void onError(Throwable e);

}
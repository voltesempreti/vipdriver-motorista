package com.vipdriver.driver.ui.activity.profile;

import com.vipdriver.driver.base.MvpPresenter;

public interface ProfileIPresenter<V extends ProfileIView> extends MvpPresenter<V> {

    void getProfile();

}

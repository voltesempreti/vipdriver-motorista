package com.vipdriver.driver.ui.activity.sociallogin;

import com.vipdriver.driver.base.MvpView;
import com.vipdriver.driver.data.network.model.Token;

public interface SocialLoginIView extends MvpView {

    void onSuccess(Token token);
    void onError(Throwable e);
}

package com.vipdriver.driver.ui.bottomsheetdialog.cancel;

import com.vipdriver.driver.base.MvpView;
import com.vipdriver.driver.data.network.model.CancelResponse;

import java.util.List;

public interface CancelDialogIView extends MvpView {

    void onSuccessCancel(Object object);
    void onError(Throwable e);
    void onSuccess(List<CancelResponse> response);
    void onReasonError(Throwable e);
}

package com.vipdriver.driver.ui.activity.notification_manager;

import com.vipdriver.driver.base.MvpPresenter;

public interface NotificationManagerIPresenter<V extends NotificationManagerIView> extends MvpPresenter<V> {
    void getNotificationManager();
}

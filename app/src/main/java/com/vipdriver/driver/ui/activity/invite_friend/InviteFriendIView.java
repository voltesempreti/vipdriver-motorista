package com.vipdriver.driver.ui.activity.invite_friend;

import com.vipdriver.driver.base.MvpView;
import com.vipdriver.driver.data.network.model.UserResponse;

public interface InviteFriendIView extends MvpView {

    void onSuccess(UserResponse response);
    void onError(Throwable e);

}

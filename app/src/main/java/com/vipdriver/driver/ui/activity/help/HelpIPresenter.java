package com.vipdriver.driver.ui.activity.help;


import com.vipdriver.driver.base.MvpPresenter;

public interface HelpIPresenter<V extends HelpIView> extends MvpPresenter<V> {

    void getHelp();
}

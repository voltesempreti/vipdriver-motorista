package com.vipdriver.driver.ui.activity.instant_ride;

import com.vipdriver.driver.base.MvpView;
import com.vipdriver.driver.data.network.model.EstimateFare;
import com.vipdriver.driver.data.network.model.TripResponse;

public interface InstantRideIView extends MvpView {

    void onSuccess(EstimateFare estimateFare);

    void onSuccess(TripResponse response);

    void onError(Throwable e);

}

package com.vipdriver.driver.ui.bottomsheetdialog.invoice_flow;

import com.vipdriver.driver.base.MvpView;

public interface InvoiceDialogIView extends MvpView {

    void onSuccess(Object object);
    void onError(Throwable e);
}

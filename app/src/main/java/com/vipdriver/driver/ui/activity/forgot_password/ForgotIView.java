package com.vipdriver.driver.ui.activity.forgot_password;

import com.vipdriver.driver.base.MvpView;
import com.vipdriver.driver.data.network.model.ForgotResponse;

public interface ForgotIView extends MvpView {

    void onSuccess(ForgotResponse forgotResponse);
    void onError(Throwable e);
}

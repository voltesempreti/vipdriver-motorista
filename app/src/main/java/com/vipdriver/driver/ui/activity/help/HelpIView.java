package com.vipdriver.driver.ui.activity.help;

import com.vipdriver.driver.base.MvpView;
import com.vipdriver.driver.data.network.model.Help;

public interface HelpIView extends MvpView {

    void onSuccess(Help object);

    void onError(Throwable e);
}

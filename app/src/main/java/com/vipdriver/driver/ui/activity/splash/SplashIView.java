package com.vipdriver.driver.ui.activity.splash;

import com.vipdriver.driver.base.MvpView;
import com.vipdriver.driver.data.network.model.CheckVersion;

public interface SplashIView extends MvpView {

    void verifyAppInstalled();

    void onSuccess(Object user);

    void onSuccess(CheckVersion user);

    void onError(Throwable e);

    void onCheckVersionError(Throwable e);
}

package com.vipdriver.driver.ui.activity.setting;

import com.vipdriver.driver.base.MvpView;

public interface SettingsIView extends MvpView {

    void onSuccess(Object o);

    void onError(Throwable e);

}

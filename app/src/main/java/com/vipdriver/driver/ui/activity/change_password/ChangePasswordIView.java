package com.vipdriver.driver.ui.activity.change_password;

import com.vipdriver.driver.base.MvpView;

public interface ChangePasswordIView extends MvpView {


    void onSuccess(Object object);
    void onError(Throwable e);
}

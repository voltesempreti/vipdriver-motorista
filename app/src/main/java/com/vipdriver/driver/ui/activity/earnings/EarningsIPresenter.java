package com.vipdriver.driver.ui.activity.earnings;


import com.vipdriver.driver.base.MvpPresenter;

public interface EarningsIPresenter<V extends EarningsIView> extends MvpPresenter<V> {

    void getEarnings();
}

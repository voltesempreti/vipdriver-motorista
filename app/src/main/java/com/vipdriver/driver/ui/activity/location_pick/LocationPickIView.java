package com.vipdriver.driver.ui.activity.location_pick;

import com.vipdriver.driver.base.MvpView;
import com.vipdriver.driver.data.network.model.AddressResponse;

/**
 * Created by santhosh@appoets.com on 19-05-2018.
 */
public interface LocationPickIView extends MvpView {

    void onSuccess(AddressResponse address);
    void onError(Throwable e);
}

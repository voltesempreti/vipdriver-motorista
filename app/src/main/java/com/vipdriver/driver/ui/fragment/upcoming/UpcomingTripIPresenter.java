package com.vipdriver.driver.ui.fragment.upcoming;


import com.vipdriver.driver.base.MvpPresenter;

public interface UpcomingTripIPresenter<V extends UpcomingTripIView> extends MvpPresenter<V> {

    void getUpcoming();

}

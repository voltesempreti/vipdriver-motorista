package com.vipdriver.driver.ui.activity.summary;


import com.vipdriver.driver.base.MvpPresenter;

public interface SummaryIPresenter<V extends SummaryIView> extends MvpPresenter<V> {

    void getSummary(String data);
}

package com.vipdriver.driver.ui.activity.wallet_detail;

import com.vipdriver.driver.base.MvpPresenter;
import com.vipdriver.driver.data.network.model.Transaction;

import java.util.ArrayList;

public interface WalletDetailIPresenter<V extends WalletDetailIView> extends MvpPresenter<V> {
    void setAdapter(ArrayList<Transaction> myList);
}

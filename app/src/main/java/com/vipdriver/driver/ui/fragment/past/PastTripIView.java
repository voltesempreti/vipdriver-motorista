package com.vipdriver.driver.ui.fragment.past;


import com.vipdriver.driver.base.MvpView;
import com.vipdriver.driver.data.network.model.HistoryList;

import java.util.List;

public interface PastTripIView extends MvpView {

    void onSuccess(List<HistoryList> historyList);
    void onError(Throwable e);
}

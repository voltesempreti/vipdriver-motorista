package com.vipdriver.driver.ui.activity.profile_update;

import com.vipdriver.driver.base.MvpView;
import com.vipdriver.driver.data.network.model.UserResponse;

public interface ProfileUpdateIView extends MvpView {

    void onSuccess(UserResponse user);

    void onSuccessUpdate(UserResponse object);

    void onError(Throwable e);

    void onSuccessPhoneNumber(Object object);

    void onVerifyPhoneNumberError(Throwable e);

}

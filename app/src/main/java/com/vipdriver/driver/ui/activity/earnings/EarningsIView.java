package com.vipdriver.driver.ui.activity.earnings;


import com.vipdriver.driver.base.MvpView;
import com.vipdriver.driver.data.network.model.EarningsList;

public interface EarningsIView extends MvpView {

    void onSuccess(EarningsList earningsLists);

    void onError(Throwable e);
}

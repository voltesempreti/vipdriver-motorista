package com.vipdriver.driver.ui.bottomsheetdialog.rating;

import com.vipdriver.driver.base.MvpView;
import com.vipdriver.driver.data.network.model.Rating;

public interface RatingDialogIView extends MvpView {

    void onSuccess(Rating rating);
    void onError(Throwable e);
}

package com.vipdriver.driver.ui.activity.reset_password;

import com.vipdriver.driver.base.MvpView;

public interface ResetIView extends MvpView{

    void onSuccess(Object object);
    void onError(Throwable e);
}

package com.vipdriver.driver.ui.activity.document;

import com.vipdriver.driver.base.MvpView;
import com.vipdriver.driver.data.network.model.DriverDocumentResponse;

public interface DocumentIView extends MvpView {

    void onSuccess(DriverDocumentResponse response);

    void onDocumentSuccess(DriverDocumentResponse response);

    void onError(Throwable e);

    void onSuccessLogout(Object object);

}

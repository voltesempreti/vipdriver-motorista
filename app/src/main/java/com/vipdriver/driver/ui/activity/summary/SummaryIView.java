package com.vipdriver.driver.ui.activity.summary;


import com.vipdriver.driver.base.MvpView;
import com.vipdriver.driver.data.network.model.Summary;

public interface SummaryIView extends MvpView {

    void onSuccess(Summary object);

    void onError(Throwable e);
}

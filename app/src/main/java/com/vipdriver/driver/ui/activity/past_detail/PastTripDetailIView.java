package com.vipdriver.driver.ui.activity.past_detail;


import com.vipdriver.driver.base.MvpView;
import com.vipdriver.driver.data.network.model.HistoryDetail;

public interface PastTripDetailIView extends MvpView {

    void onSuccess(HistoryDetail historyDetail);
    void onError(Throwable e);
}

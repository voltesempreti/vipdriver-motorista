package com.vipdriver.driver.ui.activity.add_card;

import com.vipdriver.driver.base.MvpPresenter;

public interface AddCardIPresenter<V extends AddCardIView> extends MvpPresenter<V> {

    void addCard(String stripeToken);
}

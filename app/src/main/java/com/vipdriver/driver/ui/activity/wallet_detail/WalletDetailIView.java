package com.vipdriver.driver.ui.activity.wallet_detail;

import com.vipdriver.driver.base.MvpView;
import com.vipdriver.driver.data.network.model.Transaction;

import java.util.ArrayList;

public interface WalletDetailIView extends MvpView {
    void setAdapter(ArrayList<Transaction> myList);
}

package com.vipdriver.driver.ui.activity.upcoming_detail;


import com.vipdriver.driver.base.MvpView;
import com.vipdriver.driver.data.network.model.HistoryDetail;

public interface UpcomingTripDetailIView extends MvpView {

    void onSuccess(HistoryDetail historyDetail);
    void onError(Throwable e);
}

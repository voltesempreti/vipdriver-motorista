package com.vipdriver.driver.ui.activity.profile;

import com.vipdriver.driver.base.MvpView;
import com.vipdriver.driver.data.network.model.UserResponse;

public interface ProfileIView extends MvpView {

    void onSuccess(UserResponse user);

    void onError(Throwable e);

}

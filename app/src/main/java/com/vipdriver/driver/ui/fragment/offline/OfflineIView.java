package com.vipdriver.driver.ui.fragment.offline;

import com.vipdriver.driver.base.MvpView;

public interface OfflineIView extends MvpView {

    void onSuccess(Object object);
    void onError(Throwable e);
}

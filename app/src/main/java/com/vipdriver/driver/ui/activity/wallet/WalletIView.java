package com.vipdriver.driver.ui.activity.wallet;

import com.vipdriver.driver.base.MvpView;
import com.vipdriver.driver.data.network.model.WalletMoneyAddedResponse;
import com.vipdriver.driver.data.network.model.WalletResponse;

public interface WalletIView extends MvpView {

    void onSuccess(WalletResponse response);

    void onSuccess(WalletMoneyAddedResponse response);

    void onError(Throwable e);
}

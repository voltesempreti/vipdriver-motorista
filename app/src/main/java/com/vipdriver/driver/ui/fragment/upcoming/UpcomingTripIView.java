package com.vipdriver.driver.ui.fragment.upcoming;

import com.vipdriver.driver.base.MvpView;
import com.vipdriver.driver.data.network.model.HistoryList;

import java.util.List;

public interface UpcomingTripIView extends MvpView {

    void onSuccess(List<HistoryList> historyList);
    void onError(Throwable e);
}

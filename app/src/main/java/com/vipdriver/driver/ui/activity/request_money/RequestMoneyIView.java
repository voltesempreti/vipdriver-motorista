package com.vipdriver.driver.ui.activity.request_money;

import com.vipdriver.driver.base.MvpView;
import com.vipdriver.driver.data.network.model.RequestDataResponse;

public interface RequestMoneyIView extends MvpView {

    void onSuccess(RequestDataResponse response);
    void onSuccess(Object response);
    void onError(Throwable e);

}
